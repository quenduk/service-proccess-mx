#!/usr/bin/env php
<?php

require_once __DIR__ . '/src/bootstrap.php';


use Symfony\Component\Console\Application;
use CarlosOCarvalho\MX\Console\ServiceSocketConsole;



$application = new Application();

//register consoles
$application->add( new ServiceSocketConsole );


$application->run();
